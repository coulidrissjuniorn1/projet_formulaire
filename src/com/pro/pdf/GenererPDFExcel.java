package com.pro.pdf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class GenererPDFExcel {

	public void generer(List<String> donnee) {
			
			final String fichier = "B:\\Form\\fichier_g�n�r�.pdf";
	
			// etape 1
			Document document = new Document(PageSize.A4);
		        
			try {
		            // etape 2:
		            // creation du writer -> PDF ou HTML 
		            PdfWriter.getInstance(document, new FileOutputStream(fichier));
		                      	
		            // etape 3: Ouverture du document
		            document.open();
		            
					// etape 4: Ajout du contenu au document
		            for(int i = 0; i < donnee.size(); i+=3) {
		            	int actuel = i + 3;
		            	for(int j = i; j < actuel; j++){
		            		Paragraph info = new Paragraph(donnee.get(j));
							document.add(info);
		            	}
						document.newPage();
		            }	
					
			        // etape 5: Fermeture du document
			        document.close();
			        System.out.println("Document '"+fichier+"' generated");
		           
		        }
		        catch(DocumentException de) {
		            System.err.println(de.getMessage());
		        }
		        catch(FileNotFoundException ioe) {
		            System.err.println(ioe.getMessage());
		        }catch(Exception e) {
				e.printStackTrace();
		        }	
		}
}
