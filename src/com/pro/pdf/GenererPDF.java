package com.pro.pdf;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class GenererPDF {
	
	//private final static String fichier = "B:\\Form\\fichier.pdf";
	private int id;
	private String nom;
	private String prenom;
	private String date;
	
	public GenererPDF(int id, String nom, String prenom, String date) {
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.date = date;
	}
	
	public void generer() {
		
		final String fichier = "B:\\Form\\fichier_"+this.id+".pdf";

		// etape 1
		Document document = new Document(PageSize.A4);
	        
		try {
	            // etape 2:
	            // creation du writer -> PDF ou HTML 
	            PdfWriter.getInstance(document, new FileOutputStream(fichier));
	                      	
	            // etape 3: Ouverture du document
	            document.open();
	            
				// etape 4: Ajout du contenu au document
				Paragraph nom = new Paragraph(this.nom);
				Paragraph prenom = new Paragraph(this.prenom);
				Paragraph date = new Paragraph(this.date);
				
				document.add(nom);
				document.add(prenom);
				document.add(date);
				document.add(Chunk.NEXTPAGE);
				
		        // etape 5: Fermeture du document
		        document.close();
		        System.out.println("Document '"+fichier+"' generated");
	           
	        }
	        catch(DocumentException de) {
	            System.err.println(de.getMessage());
	        }
	        catch(FileNotFoundException ioe) {
	            System.err.println(ioe.getMessage());
	        }catch(Exception e) {
			e.printStackTrace();
	        }	
	}
}
