package com.pro.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelFile {

	public List<String> read(File file) {
		List<String> sortie = new ArrayList<String>();
		try(FileInputStream fis = new FileInputStream(file); XSSFWorkbook wb = new XSSFWorkbook(fis);)
		{
			XSSFSheet sheet = wb.getSheetAt(0);	//creating a Sheet object to retrieve object
			Iterator<Row> itr = sheet.iterator();    //iterating over excel file  
			
			while (itr.hasNext())                 
			{  
				Row row = itr.next();  
				Iterator<Cell> cellIterator = row.cellIterator();   //iterating over each column  
				while (cellIterator.hasNext())   
				{  
					
					Cell cell = cellIterator.next();
					sortie.add(cell.toString());
					//System.out.println(sortie);
				}  
				//System.out.println("");  
			} 
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}catch(IOException e) {
			e.printStackTrace();
		}
		
		return sortie;		
	}
}
