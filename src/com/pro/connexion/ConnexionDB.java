package com.pro.connexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.pro.pdf.GenererPDF;

public class ConnexionDB {
	private final String url = "jdbc:postgresql://localhost:5432/formulaire";
	private final String user = "postgres";
	private final String password = "Jun144900796";
	
	Connection connection = null;
	Statement statement = null;
	ResultSet rs = null;
	
	public  void Connect() {
		
		try {
			
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(url, user, password);
			
			if(connection != null) {
				System.out.println("Connexion r�ussie !");
			}else {
				System.out.println("Connexion �chou�e !");
			}
			
		}catch(Exception e) {
			System.out.println(e);
		}
	}
	
	public void Inserer(String nom, String prenom, String date) {
		
		try {
			
			String query = "INSERT INTO personne(nom, prenom, naissance) VALUES('"+nom+"','"+prenom+"','"+date+"')";
			statement = connection.createStatement();
			statement.executeUpdate(query);
			//System.out.println("Insertion r�ussie !");
			
			String message = "Donn�es enregistr�es avec succ�s !";
			JOptionPane.showMessageDialog(null, message);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public int RecupererId(String nom, String prenom) {
		
		int id = 0;
		try {
			
			String query = "SELECT id FROM personne WHERE nom ='"+nom+"'AND prenom ='"+prenom+"'";
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			
			
			while(rs.next()) {
				id = rs.getInt("id");
				//System.out.println("Selection r�ussie !, ID = "+id);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return id;
	}
	
	public List<String> Lire(int id) {
		
		 List<String> liste = new ArrayList<String>();
		 
		try {
			
			String query = "SELECT * FROM personne WHERE id = "+id;
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			System.out.println("Selection r�ussie !");
			
			while(rs.next()) {
				liste.add(rs.getString(1));
				liste.add(rs.getString(2));
				liste.add(rs.getString(3));
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return liste;
	}
	
	public void Imprimer(int id) {
		
		String nom, prenom, date;
	 
		try {
		
		String query = "SELECT * FROM personne WHERE id = "+id;
		statement = connection.createStatement();
		rs = statement.executeQuery(query);
		//System.out.println("Selection r�ussie !");
		
			while(rs.next()) {
				
				// etape 4: Ajout du contenu au document
				nom = rs.getString("nom");
				prenom = rs.getString("prenom");
				date = rs.getString("naissance");
				
				GenererPDF genererPdf = new GenererPDF(id, nom, prenom, date);
				genererPdf.generer();
			}
		
		}catch(Exception e) {
			e.printStackTrace();
		}
	
	}	
}
