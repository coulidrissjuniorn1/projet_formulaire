package com.pro.pages;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.pro.connexion.ConnexionDB;
import com.pro.excel.ReadExcelFile;
import com.pro.pdf.GenererPDFExcel;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTextField;

//import javax.swing.JFileChooser;

public class formulaire extends JFrame {

	private JPanel contentPane;
	private static JTextField txtNom;
	private static JTextField txtPrenom;
	private static JTextField txtDate;
	
	public String fileName;
	public List<String> donnee;
	public ReadExcelFile reading;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					formulaire frame = new formulaire();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the frame.
	 */
	public formulaire() {
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 489, 368);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		panel.setBounds(0, 0, 237, 329);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Soyez la Bienvenue !!!");
		lblNewLabel.setForeground(Color.MAGENTA);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel.setBounds(21, 119, 199, 95);
		panel.add(lblNewLabel);
		
		JButton btnGenerer = new JButton("GENERER");
		btnGenerer.setBounds(83, 273, 103, 32);
		panel.add(btnGenerer);
		btnGenerer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GenererPDFExcel generer = new GenererPDFExcel();
				generer.generer(donnee);
			}
		});
		btnGenerer.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 14));
		btnGenerer.setBackground(Color.PINK);
		
		JButton btnChoisir = new JButton("...");
		btnChoisir.setBounds(55, 273, 28, 32);
		panel.add(btnChoisir);
		btnChoisir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser fichier = new JFileChooser("B:\\\\Form");
				fichier.setDialogTitle("Ouvrir un fichier EXCEL");				
				fichier.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int response = fichier.showOpenDialog(null);
				
				if(response == JFileChooser.APPROVE_OPTION) {
					File file = fichier.getSelectedFile();
					fileName = file.getName();
					
					reading = new ReadExcelFile();
					donnee = reading.read(file);
				}
			}
		});
		btnChoisir.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 14));
		btnChoisir.setBackground(Color.PINK);
		
		JButton btnValider = new JButton("VALIDER");
		btnValider.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 14));
		btnValider.setBackground(Color.GREEN);
		btnValider.setBounds(253, 274, 97, 32);
		contentPane.add(btnValider);
		btnValider.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				//connexion � la BD
				ConnexionDB connexion = new ConnexionDB();
				connexion.Connect();
				
				String nom = txtNom.getText();
				String prenom = txtPrenom.getText();
				String date = txtDate.getText();
				
				connexion.Inserer(nom, prenom, date);
				connexion.RecupererId(nom, prenom);
			}
			
		});
		
		JButton btnImprimer = new JButton("IMPRIMER");
		btnImprimer.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 14));
		btnImprimer.setBackground(Color.CYAN);
		btnImprimer.setBounds(360, 274, 103, 32);
		contentPane.add(btnImprimer);
		btnImprimer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				//connexion � la BD
				ConnexionDB connexion = new ConnexionDB();
				connexion.Connect();
				
				String nom = txtNom.getText();
				String prenom = txtPrenom.getText();
				
				int id;
				id = connexion.RecupererId(nom, prenom);
				connexion.Imprimer(id);
				
			}
			
		});
		
		JLabel lblNom = new JLabel("NOM :");
		lblNom.setForeground(Color.BLACK);
		lblNom.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 12));
		lblNom.setBounds(253, 53, 47, 14);
		contentPane.add(lblNom);
		
		txtNom = new JTextField();
		txtNom.setBounds(253, 70, 210, 32);
		contentPane.add(txtNom);
		txtNom.setColumns(10);
		
		txtPrenom = new JTextField();
		txtPrenom.setColumns(10);
		txtPrenom.setBounds(253, 141, 210, 32);
		contentPane.add(txtPrenom);
		
		JLabel lblPrenom = new JLabel("PRENOM :");
		lblPrenom.setForeground(Color.BLACK);
		lblPrenom.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 12));
		lblPrenom.setBounds(253, 124, 73, 14);
		contentPane.add(lblPrenom);
		
		txtDate = new JTextField();
		txtDate.setColumns(10);
		txtDate.setBounds(253, 212, 210, 32);
		contentPane.add(txtDate);
		
		JLabel lblDateDeNaissance = new JLabel("DATE DE NAISSANCE :");
		lblDateDeNaissance.setForeground(Color.BLACK);
		lblDateDeNaissance.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 12));
		lblDateDeNaissance.setBounds(253, 195, 155, 14);
		contentPane.add(lblDateDeNaissance);
		
		JLabel nomFichier = new JLabel("");
		nomFichier.setFont(new Font("Times New Roman", Font.PLAIN, 8));
		nomFichier.setBounds(388, 246, 85, 14);
		contentPane.add(nomFichier);
		//nomFichier.setText(fileName);
	}
}
